-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 23-Abr-2015 às 16:39
-- Versão do servidor: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `appPay`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `allowip`
--

CREATE TABLE IF NOT EXISTS `allowip` (
  `id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `issuerid` int(11) DEFAULT NULL,
  `srvtitle` varchar(255) DEFAULT NULL,
  `srvip` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(23) NOT NULL,
  `issuerid` int(11) NOT NULL,
  `invoiceid` varchar(255) NOT NULL,
  `profiletitle` varchar(255) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(23,2) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `currencytype` varchar(255) DEFAULT NULL,
  `taxforissuer` int(1) NOT NULL DEFAULT '0',
  `forcenetamount` int(1) NOT NULL DEFAULT '0',
  `paid` int(1) NOT NULL DEFAULT '0',
  `paidstatus` varchar(255) DEFAULT NULL,
  `paidbymethod` varchar(255) DEFAULT NULL,
  `canceled` int(1) NOT NULL DEFAULT '0',
  `canceledstatus` varchar(255) DEFAULT NULL,
  `refunded` int(1) NOT NULL DEFAULT '0',
  `refundedstatus` varchar(255) DEFAULT NULL,
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockedstatus` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `invoice`
--

INSERT INTO `invoice` (`id`, `issuerid`, `invoiceid`, `profiletitle`, `userid`, `title`, `description`, `amount`, `currency`, `currencytype`, `taxforissuer`, `forcenetamount`, `paid`, `paidstatus`, `paidbymethod`, `canceled`, `canceledstatus`, `refunded`, `refundedstatus`, `blocked`, `blockedstatus`) VALUES
(1, 1, '1001', 'Primary', 1, 'Credit recharge.', 'Credit recharge.', 20.00, 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(2, 1, '1001', 'Primary', 2, 'THRIVE-CHARGE-1001-90', '', 20.00, 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer`
--

CREATE TABLE IF NOT EXISTS `issuer` (
  `id` int(11) NOT NULL COMMENT 'Auto Increment, tracking number',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Business name, e.g. Apple inc.',
  `lastname` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emailsec` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordhash` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `pinhash` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `confirmedemail` varchar(255) DEFAULT NULL,
  `confirmedemailsec` varchar(255) DEFAULT NULL,
  `confirmeduser` varchar(255) DEFAULT NULL,
  `confirmedproof` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Registration issuers';

--
-- Extraindo dados da tabela `issuer`
--

INSERT INTO `issuer` (`id`, `firstname`, `lastname`, `company`, `position`, `email`, `emailsec`, `username`, `password`, `passwordhash`, `pin`, `pinhash`, `token`, `confirmedemail`, `confirmedemailsec`, `confirmeduser`, `confirmedproof`) VALUES
(1, 'Jorge', 'Amado', 'We Thrive Group .org', 'CEO', 'oliveira.jesse@gmail.com', 'mkcampanha@gmail.com', 'thrive', 'vi281186', 'vi281186', '4321', '4321', '16B73CBA5159A4609670FD6C560CCF0A9035F2AA0596B87E11B6BDAE164B2811', 'ok', 'ok', 'ok', 'ok');

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer_address`
--

CREATE TABLE IF NOT EXISTS `issuer_address` (
  `id` int(11) NOT NULL,
  `issuerid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `stateprovince` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Address issuers.';

--
-- Extraindo dados da tabela `issuer_address`
--

INSERT INTO `issuer_address` (`id`, `issuerid`, `title`, `address1`, `address2`, `city`, `stateprovince`, `postalcode`, `country`) VALUES
(1, 1, 'Primary', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer_profile`
--

CREATE TABLE IF NOT EXISTS `issuer_profile` (
  `id` int(11) NOT NULL,
  `issuerid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `businessname` varchar(255) DEFAULT NULL,
  `tin` varchar(255) DEFAULT NULL,
  `trademark` varchar(255) DEFAULT NULL,
  `slogan` varchar(255) DEFAULT NULL,
  `picture` longblob,
  `picturepath` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `websitesupport` varchar(255) DEFAULT NULL,
  `emailsupport` varchar(255) DEFAULT NULL,
  `phonesupport` varchar(255) DEFAULT NULL,
  `payinstructions` varchar(255) DEFAULT NULL COMMENT 'Payment instructions.',
  `descriptionpay` varchar(255) DEFAULT NULL COMMENT 'Description of payment',
  `addressid` int(11) DEFAULT NULL,
  `acceptedcurrency` varchar(255) DEFAULT NULL,
  `acceptedcurrencytype` varchar(255) DEFAULT NULL,
  `acceptedcountry` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `issuer_profile`
--

INSERT INTO `issuer_profile` (`id`, `issuerid`, `title`, `businessname`, `tin`, `trademark`, `slogan`, `picture`, `picturepath`, `website`, `email`, `phone`, `websitesupport`, `emailsupport`, `phonesupport`, `payinstructions`, `descriptionpay`, `addressid`, `acceptedcurrency`, `acceptedcurrencytype`, `acceptedcountry`) VALUES
(1, 1, 'Primary', 'WE thrive Group .org', NULL, 'THRIVE', 'WE thrive Group', NULL, '/1/profile/1/1.png', 'www.wetrivegroup.com', 'info@wetrivegroup.com', NULL, 'wetrivegroup.com', 'support@wetrivegroup.com', NULL, NULL, NULL, 1, 'USD', 'ISO', 'USA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `createdip` varchar(255) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `updatedip` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `doc` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordhash` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `pinhash` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emailsec` varchar(255) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `socialprofile` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `confirmed` int(1) NOT NULL DEFAULT '0',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `limited` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Registration users.';

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `created`, `createdip`, `updated`, `updatedip`, `firstname`, `lastname`, `doc`, `company`, `username`, `password`, `passwordhash`, `pin`, `pinhash`, `email`, `emailsec`, `phone1`, `phone2`, `socialprofile`, `address1`, `address2`, `city`, `state`, `postalcode`, `country`, `confirmed`, `blocked`, `limited`) VALUES
(1, '2015-04-22 00:00:00', NULL, '2015-04-22 00:00:00', NULL, 'Jesse', 'Jusoli', '987654321', 'Jesse Corp', 'jesse', 'vi281186', NULL, '1234', NULL, 'jessejusoli@gmail.com', 'friendsidc@gmail.com', '+5511949583060', NULL, 'fb.com/jessjusoli', 'Av. Paulista, 726 - Ap1303D', 'Jd. Bela Vista', 'Sao Paulo', 'Sao Paulo', '01310-100', 'Brasil', 1, 0, 0),
(2, '0000-00-00 00:00:00', '10.240.238.31', '0000-00-00 00:00:00', '10.240.238.31', 'Lilian', 'Oliveira', '1234567890', NULL, NULL, NULL, NULL, NULL, NULL, 'liliandoliveira@gmail.com', NULL, '+5524976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'Rio de janeiro', '212070-230', 'Brasil', 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allowip`
--
ALTER TABLE `allowip`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `issuer`
--
ALTER TABLE `issuer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issuer_address`
--
ALTER TABLE `issuer_address`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `issuer_profile`
--
ALTER TABLE `issuer_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allowip`
--
ALTER TABLE `allowip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(23) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `issuer`
--
ALTER TABLE `issuer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment, tracking number',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `issuer_address`
--
ALTER TABLE `issuer_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `issuer_profile`
--
ALTER TABLE `issuer_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
