-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 12-Jul-2016 às 16:13
-- Versão do servidor: 5.5.49-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `c9`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `allowip`
--

CREATE TABLE IF NOT EXISTS `allowip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `issuerid` int(11) DEFAULT NULL,
  `srvtitle` varchar(255) DEFAULT NULL,
  `srvip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(23) NOT NULL AUTO_INCREMENT,
  `issuerid` int(11) NOT NULL,
  `invoiceid` varchar(255) NOT NULL,
  `profiletitle` varchar(255) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(23,2) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `currencytype` varchar(255) DEFAULT NULL,
  `taxforissuer` int(1) NOT NULL DEFAULT '0',
  `forcenetamount` int(1) NOT NULL DEFAULT '0',
  `paid` int(1) NOT NULL DEFAULT '0',
  `paidstatus` varchar(255) DEFAULT NULL,
  `paidbymethod` varchar(255) DEFAULT NULL,
  `canceled` int(1) NOT NULL DEFAULT '0',
  `canceledstatus` varchar(255) DEFAULT NULL,
  `refunded` int(1) NOT NULL DEFAULT '0',
  `refundedstatus` varchar(255) DEFAULT NULL,
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockedstatus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Extraindo dados da tabela `invoice`
--

INSERT INTO `invoice` (`id`, `issuerid`, `invoiceid`, `profiletitle`, `userid`, `title`, `description`, `amount`, `currency`, `currencytype`, `taxforissuer`, `forcenetamount`, `paid`, `paidstatus`, `paidbymethod`, `canceled`, `canceledstatus`, `refunded`, `refundedstatus`, `blocked`, `blockedstatus`) VALUES
(1, 1, '1001', 'Primary', 1, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '20.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(2, 1, '1001', 'Primary', 2, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '20.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(3, 1, '1002', 'Primary', 2, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '20.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(4, 1, '1003', 'Primary', 2, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '20.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(5, 1, '1004', 'Primary', 3, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '20.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(6, 1, '1005', 'Primary', 3, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '20.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(7, 1, '1006', 'Primary', 3, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '20.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(8, 1, '1006', 'Primary', 4, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '20.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(9, 1, '1047', 'Primary', 5, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '60.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(10, 1, '2090', 'PrimaryXX', 7, 'Recarga de Credito', 'Recarga de Credito.', '60.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(11, 2, '2090', 'Primary', 7, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(12, 2, '2090', 'Primary', 10, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(13, 2, '2094', 'Primary', 11, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(14, 2, '2094', 'Primary', 12, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(15, 2, '2094', 'Primary', 13, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(16, 2, '2094', 'Primary', 14, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(17, 2, '7', 'Primary', 18, 'Recarga de CrÃ©dito', 'Recarga de CrÃ©dito.', '300.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(18, 2, '7', 'Primary', 19, 'Recarga de CrÃ©dito', 'Recarga de CrÃ©dito.', '300.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(19, 2, '2121', 'Primary', 20, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(20, 2, '2125', 'Primary', 21, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(21, 2, '8', 'Primary', 19, 'Recarga de CrÃ©dito', 'Recarga de CrÃ©dito.', '300.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(22, 2, '9', 'Primary', 19, 'Recarga de CrÃ©dito', 'Recarga de CrÃ©dito.', '300.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(23, 2, '2129', 'Primary', 23, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(24, 2, '10', 'Primary', 19, 'Recarga de CrÃ©dito', 'Recarga de CrÃ©dito.', '300.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(25, 2, '2137', 'Primary', 25, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(26, 2, '11', 'Primary', 19, 'Recarga de CrÃ©dito', 'Recarga de CrÃ©dito.', '300.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(27, 1, '104700', 'Primary', 26, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '60.00', 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer`
--

CREATE TABLE IF NOT EXISTS `issuer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment, tracking number',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Business name, e.g. Apple inc.',
  `lastname` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emailsec` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordhash` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `pinhash` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `confirmedemail` varchar(255) DEFAULT NULL,
  `confirmedemailsec` varchar(255) DEFAULT NULL,
  `confirmeduser` varchar(255) DEFAULT NULL,
  `confirmedproof` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Registration issuers' AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `issuer`
--

INSERT INTO `issuer` (`id`, `firstname`, `lastname`, `company`, `position`, `email`, `emailsec`, `username`, `password`, `passwordhash`, `pin`, `pinhash`, `token`, `confirmedemail`, `confirmedemailsec`, `confirmeduser`, `confirmedproof`) VALUES
(1, 'Jorge', 'Amado', 'We Thrive Group .org', 'CEO', 'oliveira.jesse@gmail.com', 'mkcampanha@gmail.com', 'thrive', 'vi281186', 'vi281186', '4321', '4321', '21072269-C3DB-444C-BB79-262BF39BB7DB', 'ok', 'ok', 'ok', 'ok'),
(2, 'Jorge', 'Amado', 'ECOSSISTEM', 'CEO', 'financeiro@ecossistem.net', 'mkcampanha@gmail.com', 'ecossistem', 'vi281186', 'vi281186', '4321', '4321', '21072269-C3DB-444C-BB79-262BF39BB7DB', 'ok', 'ok', 'ok', 'ok');

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer_address`
--

CREATE TABLE IF NOT EXISTS `issuer_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issuerid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `stateprovince` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Address issuers.' AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `issuer_address`
--

INSERT INTO `issuer_address` (`id`, `issuerid`, `title`, `address1`, `address2`, `city`, `stateprovince`, `postalcode`, `country`) VALUES
(1, 1, 'Primary', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer_profile`
--

CREATE TABLE IF NOT EXISTS `issuer_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issuerid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `businessname` varchar(255) DEFAULT NULL,
  `tin` varchar(255) DEFAULT NULL,
  `trademark` varchar(255) DEFAULT NULL,
  `slogan` varchar(255) DEFAULT NULL,
  `picture` longblob,
  `picturepath` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `websitesupport` varchar(255) DEFAULT NULL,
  `emailsupport` varchar(255) DEFAULT NULL,
  `phonesupport` varchar(255) DEFAULT NULL,
  `payinstructions` varchar(255) DEFAULT NULL COMMENT 'Payment instructions.',
  `descriptionpay` varchar(255) DEFAULT NULL COMMENT 'Description of payment',
  `addressid` int(11) DEFAULT NULL,
  `acceptedcurrency` varchar(255) DEFAULT NULL,
  `acceptedcurrencytype` varchar(255) DEFAULT NULL,
  `acceptedcountry` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `issuer_profile`
--

INSERT INTO `issuer_profile` (`id`, `issuerid`, `title`, `businessname`, `tin`, `trademark`, `slogan`, `picture`, `picturepath`, `website`, `email`, `phone`, `websitesupport`, `emailsupport`, `phonesupport`, `payinstructions`, `descriptionpay`, `addressid`, `acceptedcurrency`, `acceptedcurrencytype`, `acceptedcountry`) VALUES
(1, 1, 'Primary', 'WE thrive Group .org', NULL, 'THRIVE', 'WE thrive Group', NULL, '/1/profile/1/1.png', 'www.wetrivegroup.com', 'info@wetrivegroup.com', NULL, 'wetrivegroup.com', 'support@wetrivegroup.com', NULL, NULL, NULL, 1, 'BRL', 'ISO', 'BRL'),
(3, 1, 'Primary', 'ECOS SISTEM', NULL, 'ECOSSISTEM', 'ECOSSISTEM ', NULL, '/1/profile/1/1.png', 'www.ecossistem.net', 'financeiro@ecossistem.net', NULL, 'ecossistem.net', 'financeiro@ecossistem.net', NULL, NULL, NULL, 1, 'BRL', 'ISO', 'BRL');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `createdip` varchar(255) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `updatedip` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `doc` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordhash` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `pinhash` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emailsec` varchar(255) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `socialprofile` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `confirmed` int(1) NOT NULL DEFAULT '0',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `limited` int(1) NOT NULL DEFAULT '0',
  `birthdate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Registration users.' AUTO_INCREMENT=27 ;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `created`, `createdip`, `updated`, `updatedip`, `firstname`, `lastname`, `doc`, `company`, `username`, `password`, `passwordhash`, `pin`, `pinhash`, `email`, `emailsec`, `phone1`, `phone2`, `socialprofile`, `address1`, `address2`, `city`, `state`, `postalcode`, `country`, `confirmed`, `blocked`, `limited`, `birthdate`) VALUES
(1, '2015-04-22 00:00:00', NULL, '2015-04-22 00:00:00', NULL, 'Jesse', 'Jusoli', '987654321', 'Jesse Corp', 'jesse', 'vi281186', NULL, '1234', NULL, 'jessejusoli@gmail.com', 'friendsidc@gmail.com', '+5511949583060', NULL, 'fb.com/jessjusoli', 'Av. Paulista, 726 - Ap1303D', 'Jd. Bela Vista', 'Sao Paulo', 'Sao Paulo', '01310-100', 'Brasil', 1, 0, 0, NULL),
(2, '0000-00-00 00:00:00', '10.240.238.31', '0000-00-00 00:00:00', '10.240.238.31', 'Lilian', 'Oliveira', '1234567890', NULL, NULL, NULL, NULL, NULL, NULL, 'liliandoliveira@gmail.com', NULL, '+5524976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'Rio de janeiro', '212070-230', 'Brasil', 1, 0, 0, NULL),
(3, '0000-00-00 00:00:00', '10.240.25.150', '0000-00-00 00:00:00', '10.240.25.150', 'LilianX', 'OliveiraX', '91234567890', NULL, NULL, NULL, NULL, NULL, NULL, 'liliandoliveira+two@gmail.com', NULL, '+5524976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'Rio de janeiro', '212070-230', 'Brasil', 1, 0, 0, NULL),
(4, '0000-00-00 00:00:00', '10.240.213.116', '0000-00-00 00:00:00', '10.240.213.116', 'Eber', 'Felipe', '81234567890', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcampanha+Eber@gmail.com', NULL, '+5524976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'Rio de janeiro', '212070-230', 'Brasil', 1, 0, 0, NULL),
(5, '0000-00-00 00:00:00', '10.240.169.177', '0000-00-00 00:00:00', '10.240.169.177', 'JessÃ©', 'JoÃ£o', '8123456789340', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcampanhaCelisssa@gmail.com', NULL, '+5524976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, NULL),
(6, '0000-00-00 00:00:00', '10.240.255.126', '0000-00-00 00:00:00', '10.240.255.126', 'Primeiro Nome', 'Ultimo nome', 'Documento fiscal / CPF / Social Security NUmber (SSN)', NULL, NULL, NULL, NULL, NULL, NULL, 'E-mail', NULL, 'Numero de Telefone', 'Numero de Telefone', '', 'Logradouro com numero e complemento', 'Bairro', 'Cidade', 'Estado / Provincia', 'Codigo postal / CEP / postalcode/zipcode', 'Pais', 1, 0, 0, NULL),
(7, '0000-00-00 00:00:00', '10.240.212.249', '0000-00-00 00:00:00', '10.240.212.249', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcampanha+alpha@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(8, '0000-00-00 00:00:00', '10.240.6.30', '0000-00-00 00:00:00', '10.240.6.30', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+alpha@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(9, '0000-00-00 00:00:00', '10.240.212.249', '0000-00-00 00:00:00', '10.240.212.249', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+beta@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(10, '0000-00-00 00:00:00', '10.240.255.126', '0000-00-00 00:00:00', '10.240.255.126', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+gamma@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(11, '0000-00-00 00:00:00', '10.240.169.177', '0000-00-00 00:00:00', '10.240.169.177', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+delta@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(12, '0000-00-00 00:00:00', '10.240.169.177', '0000-00-00 00:00:00', '10.240.169.177', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+epsilon@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(13, '0000-00-00 00:00:00', '10.240.213.116', '0000-00-00 00:00:00', '10.240.213.116', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+zeta@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(14, '0000-00-00 00:00:00', '10.240.255.126', '0000-00-00 00:00:00', '10.240.255.126', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+eta@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(15, '0000-00-00 00:00:00', '10.240.169.177', '0000-00-00 00:00:00', '10.240.169.177', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+theta@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(16, '0000-00-00 00:00:00', '10.240.255.126', '0000-00-00 00:00:00', '10.240.255.126', 'Alpha', 'Oliveira', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+iota@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(17, '0000-00-00 00:00:00', '10.240.255.126', '0000-00-00 00:00:00', '10.240.255.126', 'Rosali', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+kappa@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(18, '0000-00-00 00:00:00', '10.240.169.177', '0000-00-00 00:00:00', '10.240.169.177', 'Primeiro Nome', 'Ultimo nome', '12345678901', NULL, NULL, NULL, NULL, NULL, NULL, 'email@email.com', NULL, '83999998888', '', '', 'Rua Tal, 190 - apartamento 101', 'Bessa', 'JoÃ£o Pessoa', 'ParaÃ­ba', '58000000', 'Brasil', 1, 0, 0, NULL),
(19, '0000-00-00 00:00:00', '10.240.212.249', '0000-00-00 00:00:00', '10.240.212.249', 'Sdfdsfsdf', 'Sdfsdf', '12345678901', NULL, NULL, NULL, NULL, NULL, NULL, 'sdfsdf@fsdfsdf.com', NULL, '83999998888', '', '', 'Rua Tal, 190 - apartamento 101', 'Bessa', 'JoÃ£o Pessoa', 'ParaÃ­ba', '58000000', 'Brasil', 1, 0, 0, NULL),
(20, '0000-00-00 00:00:00', '10.240.25.150', '0000-00-00 00:00:00', '10.240.25.150', 'Rosali', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+omega@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(21, '0000-00-00 00:00:00', '10.240.6.30', '0000-00-00 00:00:00', '10.240.6.30', 'Rosali', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+otango@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(22, '0000-00-00 00:00:00', '10.240.255.126', '0000-00-00 00:00:00', '10.240.255.126', 'Carlos', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+oxtango@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(23, '0000-00-00 00:00:00', '10.240.169.177', '0000-00-00 00:00:00', '10.240.169.177', 'Carlos', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+oxxtango@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(24, '0000-00-00 00:00:00', '10.240.213.116', '0000-00-00 00:00:00', '10.240.213.116', 'Roberto', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+oxxxtango@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(25, '0000-00-00 00:00:00', '10.240.6.30', '0000-00-00 00:00:00', '10.240.6.30', 'Roberto', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+oxxxtangox@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, NULL),
(26, '0000-00-00 00:00:00', '10.240.0.204', '0000-00-00 00:00:00', '10.240.0.204', 'Jessa', 'Joao', '8123456789', NULL, NULL, NULL, NULL, NULL, NULL, 'jjo123@gmail.com', NULL, '+5524976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, '0000-00-00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
